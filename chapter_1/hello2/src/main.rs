fn greet_world() {
    println!("Hello, world!"); // ! means this is a marco: a fancy function

    let southern_germany = "Grüß Gott!";
    let japan = "ハロー・ワールド";

    let regions = [southern_germany, japan];
    for region in regions.iter() {
        println!("{}", &region)
    }
}

fn main() {
    greet_world();
}
