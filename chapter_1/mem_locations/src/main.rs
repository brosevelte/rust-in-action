use std::rc::Rc;
use std::sync::{Arc, Mutex};

fn main() {
    let a = 10; // Integer on the STACK
    let b = Box::new(20); // Integer on the HEAP
    let c = Rc::new(Box::new(30)); // Boxed Integer wrapped in a Reference Counter
    let d = Arc::new(Mutex::new(40)); // Integer wrapped in an Atomic reference counter,
                                      // protected by a mutual exclusion lock

    println!("a: {:?}, b: {:?}, c: {:?}, d: {:?}", a, b, c, d);
}
