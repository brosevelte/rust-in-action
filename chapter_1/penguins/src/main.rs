fn main() {
    let penguin_data = "\
    common name,length (cm)
    Little penguin,33
    Yellow-eyed penguin,65
    Fiordland penguin,60
    Invalid,data
    ";

    let records = penguin_data.lines();

    for (i, record) in records.enumerate() {
        if i == 0 || record.trim().len() == 0 {
            continue;
        }

        let fields: Vec<_> = record // Create a vector, "_" means infer type
            .split(',')
            .map(|field| field.trim()) // This is a "closure" (unnamed function, lambda)
            .collect();

        if cfg!(debug_assertions) {
            // Compiler skips if in prod mode (cargo run -r)
            // Prints to stderr

            // Macros return CODE not data,
            // {} says to represent the value as a string
            // {:?} is the default representation of the type
            eprintln!("Debug: {:?} -> {:?}", record, fields);
        }

        let name = fields[0];
        // Attempt to parse fields[1] as a float32.
        // if successful assign to length
        if let Ok(length) = fields[1].parse::<f32>() {
            println!("{}, {}cm", name, length);
        } else {
            eprintln!("Unable to parse length.");
        }
    }
}
